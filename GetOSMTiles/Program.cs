﻿using CommandLine;
using GetOSMTiles.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace GetOSMTiles
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {

            await Parser.Default
              .ParseArguments<Options>(args)
              .WithNotParsed(e =>
              {
                  Console.Error.WriteLine(e.ToString());
                  Environment.Exit(1);
              })
              .WithParsedAsync(async o =>
              {
                  await RunAsync(o).ConfigureAwait(false);
              });

            return 0;
        }

        private static async Task RunAsync(Options options)
        {
            var targetDirectory = new DirectoryInfo(options.LocalStore);

            if (options.Clean)
            {
                CleanDirectory(targetDirectory);
            }

            var osmTilesGetter = new OSMTilesGetter(options.ServiceUrl, options.Pattern, targetDirectory);

            await MeasureTimeAsync(async () => await osmTilesGetter.GetAllTilesAsync(
                options.MinLevel,
                options.MaxLevel,
                options.MinLat,
                options.MaxLat,
                options.MinLong,
                options.MaxLong,
                options.Tasks).ConfigureAwait(false)
                ).ConfigureAwait(false);
        }

        private static async Task MeasureTimeAsync(Func<Task> asyncAction)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            await asyncAction().ConfigureAwait(false);
            stopWatch.Stop();

            Console.WriteLine($"Elapsed time:{stopWatch.Elapsed}");

        }

        private static void CleanDirectory(DirectoryInfo directory)
        {
            if (directory.Exists)
            {
                directory.Delete(true);
            }
        }

    }
}
