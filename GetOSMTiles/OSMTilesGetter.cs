﻿using GetOSMTiles.Helpers;
using GetOSMTiles.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace GetOSMTiles
{
    public interface IOSMTilesGetter
    {
        Task GetAllTilesAsync(int minLevel, int maxLevel, double minLatitude, double maxLatitude, double minLongitude, double maxLongitude, int tasks);
    }

    internal sealed class OSMTilesGetter : IOSMTilesGetter
    {
        private readonly string _serviceUrl;
        private readonly string _pattern;
        private readonly DirectoryInfo _localStoreDirectoryInfo;
        private readonly HttpClient _httpClient;

        public OSMTilesGetter(string serviceUrl, string pattern, DirectoryInfo localStoreDirectoryInfo)
        {
            _serviceUrl = serviceUrl;
            _pattern = pattern;
            _localStoreDirectoryInfo = localStoreDirectoryInfo;
            _httpClient = HttpClientHelper.CreateHttpClient();
        }


        public async Task GetAllTilesAsync(int minLevel, int maxLevel, double minLatitude, double maxLatitude, double minLongitude, double maxLongitude, int tasks)
        {
            Console.WriteLine("Get OSM tiles START");

            List<TileId> failedTiles = new List<TileId>();

            var allTilesToRetrieve = PreGetTilesHelper.PreGetAllTiles(minLevel, maxLevel, minLatitude, maxLatitude, minLongitude, maxLongitude);

            var taskRepartitor = new TaskRepartitor(tasks);

            var enumerator = allTilesToRetrieve.GetEnumerator();
            object key = new object();

            await taskRepartitor.RunAsync(
                () =>
                {
                    lock (key)
                    {
                        return enumerator.MoveNext() ? enumerator.Current : null;
                    }
                },
                async tileId =>
                {
                    if (!await CheckAndGetTileAsync(tileId).ConfigureAwait(false))
                    {
                        failedTiles.Add(tileId);
                    }
                }
            ).ConfigureAwait(false);

            Console.WriteLine("Get OSM tiles END");

            foreach (var failedTile in failedTiles)
            {
                Console.WriteLine($"ERROR : tile level {failedTile.Level}, X {failedTile.X} Y {failedTile.Y}");
            }
        }


        private async Task<bool> CheckAndGetTileAsync(TileId tileId)
        {

            var suffix = string.Format(_pattern, tileId.Level, tileId.X, tileId.Y);

            var localPath = $"{_localStoreDirectoryInfo.FullName}{suffix}";

            if (File.Exists(localPath))
            {
                Console.WriteLine($"SKIPPED : Tile {suffix} already saved. ({tileId.Percentage:P3} of level {tileId.Level})");
            }
            else
            {
                Stream stream;
                try
                {
                    var url = $"{_serviceUrl}{suffix}";
                    stream = await _httpClient.GetStreamAsync(url).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"ERROR : Tile {suffix} failed: {ex.Message}");
                    return false;
                }

                Directory.CreateDirectory(Path.GetDirectoryName(localPath));

                using (FileStream fs = new FileStream(localPath, FileMode.Create))
                {
                    stream.CopyTo(fs);
                }
                Console.WriteLine($"OK : Tile {suffix} saved. ({tileId.Percentage:P3} of level {tileId.Level})");

            }
            return true;
        }

    }


}
