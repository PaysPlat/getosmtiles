﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GetOSMTiles
{
    internal sealed class TaskRepartitor
    {
        private readonly int _slotsCount;

        public TaskRepartitor(int slotsCount)
        {
            _slotsCount = slotsCount;
        }

        public async Task RunAsync<T>(Func<T> funcGetNextItem, Func<T, Task> asyncAction)
        {
            if (funcGetNextItem == null)
            {
                throw new ArgumentNullException(nameof(funcGetNextItem));
            }

            if (asyncAction == null)
            {
                throw new ArgumentNullException(nameof(asyncAction));
            }

            var runners = new Task[_slotsCount];

            for (var i = 0; i < _slotsCount; i++)
            {
                runners[i] = Task.Run(async () =>
                {
                    var endReached = false;
                    while (!endReached)
                    {
                        var item = funcGetNextItem();

                        if (item == null)
                        {
                            endReached = true;
                            break;
                        }
                        await asyncAction(item).ConfigureAwait(false);
                    }
                });
            }

            await Task.WhenAll(runners).ConfigureAwait(false);
        }


        public async Task BatchRunAsync<T>(Func<T> funcGetNextItem, Func<T, Task> asyncAction)
        {
            if (funcGetNextItem == null)
            {
                throw new ArgumentNullException(nameof(funcGetNextItem));
            }

            if (asyncAction == null)
            {
                throw new ArgumentNullException(nameof(asyncAction));
            }

            var endReached = false;
            while (!endReached)
            {
                var subItems = new List<T>(_slotsCount);
                for (var i = 0; i < _slotsCount; i++)
                {
                    var item = funcGetNextItem();
                    if (item == null)
                    {
                        endReached = true;
                        break;
                    }
                    subItems.Add(item);
                }

                //await Parallel.ForEachAsync(subItems, async (o, ct) => await asyncAction(o).ConfigureAwait(false)).ConfigureAwait(false);

                await Task.WhenAll(
                    subItems.Select(async o => await asyncAction(o).ConfigureAwait(false))
                    ).ConfigureAwait(false);
            }
        }
    }
}