﻿using CommandLine;

namespace GetOSMTiles.Models
{
    public sealed class Options
    {
        [Option(shortName: 's', Default = "https://a.tile.openstreetmap.fr/osmfr", Required = false, HelpText = "Server base url where to get tiles")]
        public string ServiceUrl { get; set; }

        [Option(shortName: 'p', Default = "/{0:D1}/{1:D4}/{2:D4}.png", Required = false, HelpText = "Pattern of tile with Level => {0}, X => {1}, Y => {2}")]
        public string Pattern { get; set; }


        [Option(shortName: 'd', Required = true, HelpText = "Local folder where to store tiles")]
        public string LocalStore { get; set; }

        [Option(shortName: 'z', Default = 0, Required = false, HelpText = "Minimum zoom level")]
        public int MinLevel { get; set; }

        [Option(shortName: 'Z', Default = 19, Required = false, HelpText = "Maximum zoom level")]
        public int MaxLevel { get; set; }

        [Option(shortName: 'x', Default = -180, Required = false, HelpText = "Minimum longitude")]
        public double MinLong { get; set; }

        [Option(shortName: 'X', Default = 180, Required = false, HelpText = "Maximum longitude")]
        public double MaxLong { get; set; }


        [Option(shortName: 'y', Default = -90, Required = false, HelpText = "Minimum latitude")]
        public double MinLat { get; set; }

        [Option(shortName: 'Y', Default = 90, Required = false, HelpText = "Maximum latitude")]
        public double MaxLat { get; set; }


        [Option(shortName: 't', Default = 8, Required = false, HelpText = "Number of concurrent http requests tasks")]
        public int Tasks { get; set; }

        [Option(shortName: 'c', Default = false, Required = false, HelpText = "Clean target directory at startup")]
        public bool Clean { get; set; }


    }
}
