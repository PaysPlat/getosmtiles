﻿namespace GetOSMTiles.Models
{
    internal class TileId
    {

        public int Level { get; }

        public int X { get; }

        public int Y { get; }
        public double Percentage { get; }

        public TileId(int level, int x, int y, double percentage)
        {
            Level = level;
            X = x;
            Y = y;
            Percentage = percentage;
        }

    }
}