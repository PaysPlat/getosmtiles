﻿using GetOSMTiles.Models;
using System;
using System.Collections.Generic;

namespace GetOSMTiles.Helpers
{
    internal static class PreGetTilesHelper
    {
        public static IEnumerable<TileId> PreGetAllTiles(int minLevel, int maxLevel, double minLatitude, double maxLatitude, double minLongitude, double maxLongitude)
        {
            for (int i = minLevel; i <= maxLevel; i++)
            {
                foreach (var tileId in PreGetTilesOfLevel(i, minLatitude, maxLatitude, minLongitude, maxLongitude))
                {
                    yield return tileId;
                }
            }
        }

        private static IEnumerable<TileId> PreGetTilesOfLevel(int level, double minLatitude, double maxLatitude, double minLongitude, double maxLongitude)
        {
            int minLine = ComputeLine(level, maxLatitude);
            int maxline = ComputeLine(level, minLatitude);
            int minColumn = ComputeColumn(level, minLongitude);
            int maxColumn = ComputeColumn(level, maxLongitude);

            for (int i = minColumn; i <= maxColumn; i++)
            {
                for (int j = minLine; j <= maxline; j++)
                {
                    var percentage = (i - minColumn + ((double)j + 1 - minLine) / (maxline - minLine + 1)) / (maxColumn - minColumn + 1);
                    var tileId = new TileId(level, i, j, percentage);
                    yield return tileId;
                }
            }
        }

        private static int ComputeColumn(int level, double longitude)
        {
            int tileCount = 1 << level;

            var rawColumn = Math.Floor((longitude + 180.0) / 360.0 * tileCount);
            if (rawColumn < 0)
            {
                return 0;
            }
            if (rawColumn > tileCount - 1)
            {
                return tileCount - 1;
            }
            return (int)rawColumn;
        }

        private static int ComputeLine(int level, double latitude)
        {
            var tileCount = 1 << level;

            var latRad = latitude * Math.PI / 180;
            var rawLine = Math.Floor((1 - Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad)) / Math.PI) / 2 * tileCount);
            if (rawLine<0)
            {
                return 0;
            }
            if (rawLine>tileCount-1)
            {
                return tileCount - 1;
            }
            return (int)rawLine;
        }

    }
}
