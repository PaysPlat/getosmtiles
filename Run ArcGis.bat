set server=https://services.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/WMTS/tile/1.0.0/World_Imagery/default/default028mm
set pattern=/{0}/{2}/{1}.jpg
set directory=C:\Tiles\ImgArcGis
set proc=8

E:\Gitlab\getosmtiles\GetOSMTiles\bin\Release\net6.0\GetOSMTiles.exe -s %server% -p %pattern% -t %proc% -d %directory% -z 0 -Z 9
E:\Gitlab\getosmtiles\GetOSMTiles\bin\Release\net6.0\GetOSMTiles.exe -s %server% -p %pattern% -t %proc% -d %directory% -z 10 -Z 13 -x -5 -X 8.25 -y 42.25 -Y 51
E:\Gitlab\getosmtiles\GetOSMTiles\bin\Release\net6.0\GetOSMTiles.exe -s %server% -p %pattern% -t %proc% -d %directory% -z 14 -Z 16 -x 0 -X 2.5 -y 43 -Y 44
E:\Gitlab\getosmtiles\GetOSMTiles\bin\Release\net6.0\GetOSMTiles.exe -s %server% -p %pattern% -t %proc% -d %directory% -z 17 -Z 20 -x 1.3 -X 1.6 -y 43.5 -Y 43.7

pause